ifeq ($(JHBUILD_LIB),)
	JHBUILD_LIB=${HOME}/jhbuild/install/lib
endif

ifeq (${RUN_JS_TEST},)
	ifeq (${XDG_CACHE_HOME},)
		XDG_CACHE_HOME=${HOME}/.cache
	endif
	RUN_JS_TEST=${XDG_CACHE_HOME}/jhbuild/build/gnome-shell/src/run-js-test
endif

ifeq ($(FILTER),)
	FILTER=".*"
endif

STOP_ON_FAIL=""
ifneq ($(STOP),)
	STOP_ON_FAIL="-s"
endif

DEBUGCOMMAND=
ifneq ($(DEBUG),)
	DEBUGCOMMAND=libtool --mode=execute gdb --args
endif

FILES=$(shell find -iname '*.js' -and -not \( -name 'run.js' -or -path ./third_party/\* \))

GJS_PATH=.
GI_TYPELIB_PATH=${JHBUILD_LIB}/gnome-shell:${JHBUILD_LIB}/mutter
LD_LIBRARY_PATH=${JHBUILD_LIB}

.PHONY: test check
help:
	@echo "Following targets are available:"
	@echo "\thelp      show this help (default action if no target specified)"
	@echo "\ttest      run unit tests"
	@echo "\tcheck     perform jshint checks on project code"
	@echo "\tbeautify  style the code with js-beautify"
	@echo "\tdep       download and setup external depencies"
	@echo "\tclean     remove all temporary files (*.rej, *.orig, *.porig, *~)"
	@echo "\trealclean remove all temporary files and external dependencies"
	@echo "\trelease   tags repo with new version and creates a new zip file for distribution"
	@echo "\tvalidate  validates settings.json file"
	@echo "\txml       creates settings xml file, based on json file"
	@echo "\tschema    compiles settings xml into schema"
	@echo "\tinstall   prepares directory to be used as extension in gnome-shell"

test:
	LD_LIBRARY_PATH=${LD_LIBRARY_PATH} GJS_PATH=${GJS_PATH} GI_TYPELIB_PATH=${GI_TYPELIB_PATH} ${DEBUGCOMMAND} ${RUN_JS_TEST} ./unit/run.js --gjsunit_filter ${FILTER} ${STOP_ON_FAIL}

clean:
	@find \( -name '*.rej' -or -name '*.orig' -or -name '*.porig' -or -name '*~' \) -exec rm -v '{}' \;

realclean: clean
	@git submodule deinit --all
	@rm -rfv schemas

check:
	@jshint -c .jshintrc $(FILES)

beautify:
	@find \( -name '*.js' -and -not \( -name 'run.js' -or -path ./third_party/\* \) \) -exec python jsbeautify.py '{}' \;

release:
	@./make_release.sh

dep:
	@./get_libs.sh

validate:
	@python ./validate_settings.py

xml:
	@python ./json2schema.py

schema: xml
	@glib-compile-schemas schemas

install: realclean dep schema
