#!/usr/bin/env python
from jsonschema import Draft4Validator
import json

schema = json.load(open('settings_schema.json'))
toValidate = json.load(open('settings.json'))

v = Draft4Validator(schema)

for error in sorted(v.iter_errors(toValidate), key=str):
    print('{error.instance}: {error.message}'.format(error=error))
    if (len(error.context) > 0):
        for context in error.context:
            print('\t{context.message}'.format(context=context))
    print('--')

