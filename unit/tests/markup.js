const GLib = imports.gi.GLib;
const Gdk = imports.gi.Gdk;
const Markup = imports.markup;
const Unit = imports.third_party.gjsunit.gjsunit;

var suite = new Unit.Suite('Markup');

suite.addTest('initialization, no parameters', function() {
    let markup = new Markup.Markup();
    Unit.assertFalse(markup.bold);
    Unit.assertFalse(markup.italic);
    Unit.assertFalse(markup.changeColor);
    Unit.assertEquals('rgb(0,0,0)', markup.color.to_string());
});

suite.addTest('initialization with variant', function() {
    let value = [GLib.Variant.new_boolean(true), GLib.Variant.new_string('rgb(100,200,100)'), GLib.Variant.new_boolean(
        false), GLib.Variant.new_boolean(true)];
    let tuple = GLib.Variant.new_tuple(value, value.length);

    let markup = new Markup.Markup(tuple);
    Unit.assertFalse(markup.bold);
    Unit.assertTrue(markup.italic);
    Unit.assertTrue(markup.changeColor);
    let rgba = new Gdk.RGBA();
    rgba.parse('rgb(100,200,100)');
    Unit.assertEquals(rgba.to_string(), markup.color.to_string());
});

suite.addTest('initialization with non-variant', function() {
    let markup = new Markup.Markup('bla');
    Unit.assertFalse(markup.bold);
    Unit.assertFalse(markup.italic);
    Unit.assertFalse(markup.changeColor);
    Unit.assertEquals('rgb(0,0,0)', markup.color.to_string());
});

suite.addTest('initialization with wrong variant', function() {
    let value = [GLib.Variant.new_boolean(true), GLib.Variant.new_boolean(false)];
    let markup = new Markup.Markup(GLib.Variant.new_tuple(value, value.length));
});

suite.addTest('initialization with invalid color string', function() {
    let value = [GLib.Variant.new_boolean(true), GLib.Variant.new_string('some color'), GLib.Variant.new_boolean(
        false), GLib.Variant.new_boolean(true)];
    let tuple = GLib.Variant.new_tuple(value, value.length);
    let markup = new Markup.Markup(tuple);
});

suite.addTest('modify existing', function() {
    let markup = new Markup.Markup();
    Unit.assertFalse(markup.bold);
    Unit.assertFalse(markup.italic);
    Unit.assertFalse(markup.changeColor);
    Unit.assertEquals('rgb(0,0,0)', markup.color.to_string());
    markup.bold = true;
    markup.italic = true;
    markup.changeColor = true;
    let rgba = new Gdk.RGBA();
    rgba.parse('rgb(1,2,3)');
    markup.color = rgba;
    Unit.assertTrue(markup.bold);
    Unit.assertTrue(markup.italic);
    Unit.assertTrue(markup.changeColor);
    Unit.assertEquals('rgb(1,2,3)', markup.color.to_string());
});

suite.addTest('set color from string', function() {
    let markup = new Markup.Markup();
    markup.setColorFromString('rgb(100,150,200)');
    Unit.assertFalse(markup.bold);
    Unit.assertFalse(markup.italic);
    Unit.assertFalse(markup.changeColor);
    Unit.assertEquals('rgb(100,150,200)', markup.color.to_string());
});

suite.addTest('set color from wrong string', function() {
    let markup = new Markup.Markup();
    markup.setColorFromString('blaargh');
    Unit.assertFalse(markup.bold);
    Unit.assertFalse(markup.italic);
    Unit.assertFalse(markup.changeColor);
    Unit.assertEquals('rgb(0,0,0)', markup.color.to_string());
});

suite.addTest('set color from wrong type', function() {
    let markup = new Markup.Markup();
    markup.setColorFromString(true);
    Unit.assertFalse(markup.bold);
    Unit.assertFalse(markup.italic);
    Unit.assertFalse(markup.changeColor);
    Unit.assertEquals('rgb(0,0,0)', markup.color.to_string());
});

suite.addTest('convert to variant', function() {
    let value = [GLib.Variant.new_boolean(true), GLib.Variant.new_string('rgb(100,200,100)'), GLib.Variant.new_boolean(
        false), GLib.Variant.new_boolean(true)];
    let tuple = GLib.Variant.new_tuple(value, value.length);

    let markup = new Markup.Markup(tuple);
    let variant = markup.toVariant();
    Unit.assertEquals(tuple.get_type_string(), variant.get_type_string());
    let unpack = variant.deep_unpack();
    Unit.assertEquals(value[0].unpack(), unpack[0]);
    Unit.assertEquals(value[1].unpack(), unpack[1]);
    Unit.assertEquals(value[2].unpack(), unpack[2]);
    Unit.assertEquals(value[3].unpack(), unpack[3]);
});

/* vi: set expandtab tabstop=4 shiftwidth=4: */
