#!/usr/bin/gjs
const GjsUnit = imports.third_party.gjsunit.gjsunit;
const optparse = imports.third_party.optparse.lib.optparse;
const message_prefix = '\u001B[33m';
const reset_suffix = '\u001B[39m';
const Override = imports.unit.jsOverride.misc.extensionUtils;

let switches = [
    ['-f', '--gjsunit_filter STRING',
            'Regex to filter tests. Specify multiple separated by \':\', prefix with \'_\' for negative filtering.' +
            ' Seperate testsuite and name with a dot. Spaces in names should be escaped. ' +
            ' Example: \'-f include.*me:me.*too:-not.*me:suite\.name'
    ],
    ['-s', '--gjsunit_stop', 'Stop execution on first failed test'],
    ['-H', '--gjsunit_help', 'Show this help message']
];

let parser = new optparse.optparse.OptionParser(switches),
    print_summary = true,
    first_arg;
parser.banner = '';

parser.on('gjsunit_help', function() {
    print(parser.toString());
    print_summary = false;
    System.exit(0);
});

parser.on('gjsunit_filter', function(name, value) {
    print(message_prefix + 'Using filter: ' + value + reset_suffix);
    let tempFilters = value.split(':');
    for (let i in tempFilters) {
        if (tempFilters.hasOwnProperty(i)) {
            if (tempFilters[i][0] == '_') {
                GjsUnit.instance.addFilter(new RegExp(tempFilters[i].substring(1)), true);
                continue;
            }
            GjsUnit.instance.addFilter(tempFilters[i], false);
        }
    }
});

parser.on('gjsunit_stop', function() {
    GjsUnit.instance.stopOnFail = true;
});

parser.parse(ARGV);

imports.misc.extensionUtils = Override;

// By importing here the suites, we automatically create the tests and add them to the runner
imports.unit.tests.formatter;
imports.unit.tests.jsTextFile;
imports.unit.tests.markup;
imports.unit.tests.settings;

// Get the runner
if (GjsUnit.instance.run() != 0) {
	throw new Error("Not all tests run correctly. This is not really an error, just to generate an exit code != 0");
}
